/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mhaji <mhaji@student.42wolfsburg.de>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/31 09:24:02 by mhaji             #+#    #+#             */
/*   Updated: 2022/04/06 01:42:42 by mhaji            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(char *s1, char *s2)
{
	int	i;
	int	d;

	i = 0;
	d = 0;
	while ((s1[i] != 0) || (s2[i] != 0))
	{
		d = s1[i] - s2[i];
		i++;
		if (d != 0)
			break ;
	}
	return (d);
}

// #include <string.h>
// #include <stdio.h>

// int	main(void)
// {
// 	char	s1[] = "Munir";
// 	char	s2[] = "MuniR";
// 	int		i;
// 	int		j;

// 	i = strcmp(s1, s2);
// 	j = ft_strcmp(s1, s2);
// 	printf("This is builtin %d and \nThis is mine %d \n", i, j);
// }
