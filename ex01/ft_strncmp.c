/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mhaji <mhaji@student.42wolfsburg.de>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/31 09:54:31 by mhaji             #+#    #+#             */
/*   Updated: 2022/04/06 01:46:45 by mhaji            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	i;
	int				d;

	i = 0;
	d = 0;
	while (((s1[i] != 0) || (s2[i] != 0)) && i < n)
	{
		d = s1[i] - s2[i];
		i++;
		if (d != 0)
			break ;
	}
	return (d);
}

// #include <string.h>
// #include <stdio.h>

// int	main(void)
// {
// 	char	s1[] = "abcde";
// 	char	s2[] = "abcde3";
// 	int		i;
// 	int		j;

// 	i = strncmp(s1, s2, 6);
// 	j = ft_strncmp(s1, s2, 6);
// 	printf("This is builtin %d and \nThis is mine %d \n", i, j);
// }
