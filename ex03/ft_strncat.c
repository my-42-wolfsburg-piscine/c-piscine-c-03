/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mhaji <mhaji@student.42wolfsburg.de>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/31 11:04:56 by mhaji             #+#    #+#             */
/*   Updated: 2022/04/06 01:58:53 by mhaji            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	unsigned int	count_dest;
	unsigned int	i;

	count_dest = 0;
	i = 0;
	while (dest[count_dest] != 0)
		count_dest++;
	while ((src[i] != 0) && i < nb)
	{
		dest[count_dest + i] = src[i];
		i++;
	}
	dest[count_dest + i] = '\0';
	return (dest);
}

// #include <stdio.h>
// //#include <string.h>
// int	main(void)
// {
// 	char	src[] = "This is source";
// 	char	dest[] = "This is sourc";

// 	//ft_strncat(dest,src,4);
// 	//strncat(dest,src,4);
// 	//printf("\n%s\n",dest);
// 	printf("\n%s\n", ft_strncat(dest, src, 4));
// 	printf("\n%s\n", dest);
// }
