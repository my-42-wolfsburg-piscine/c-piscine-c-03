/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mhaji <mhaji@student.42wolfsburg.de>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/31 11:28:59 by mhaji             #+#    #+#             */
/*   Updated: 2022/04/06 01:52:21 by mhaji            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	int	count_dest;
	int	count_src;

	count_dest = 0;
	count_src = 0;
	while (dest[count_dest] != 0)
		count_dest++;
	while (src[count_src] != 0)
	{
		dest[count_dest] = src[count_src];
		count_dest++;
		count_src++;
	}
	dest[count_dest] = '\0';
	return (dest);
}

// #include <stdio.h>
// //#include <string.h>

// int	main(void)
// {
// 	char	src[] = "This is source";
// 	char	dest[] = "This is dest";

// 	printf("\n%s\n", ft_strcat(dest, src));
// 	//printf("\n%s\n", strcat(dest,src));
// 	printf("\n%s\n", dest);
// }
